package test;

import Kronas.Registration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;

public class KronasTest extends BaseConfig {

    public Registration userCanReg;

    @BeforeMethod
    public void beforeTest(){
        userCanReg  = new Registration();
    }
//    $(By.xpath("//div[text()='Login']")).shouldBe(visible);//пользователь увидел кнопку личный кабинет
//    ClickLkBtn();
    //пользователь увидел кнопку регистрации+
    //пользователь нажал кнопку регистрация+
    //Пользователь увидел блок регистрация.+
    //пользователь заполнил поле имя+
    //пользователь заполнил поле email валидными данными+
    //пользователь заполнил поле пароль валидными данными+
    //пользователь заполнил поле подтвердить пароль валидными данными+
    //пользователь нажал на кнопку Регистрироваться
    //Пользователь увидел сообщение об успешной регистрации

    @Test
    public void start(){
       open(blogUrl);
       userCanReg.lkBtn.shouldBe(visible);
       userCanReg.ClickLkBtn();
       userCanReg.form.shouldBe(visible);
       userCanReg.valnameField("test");
       userCanReg.valemailField("anniegarett@gmail.com");
       userCanReg.valpasswordField("qwerty123");
       userCanReg.valdoublePas("qwerty123");
       userCanReg.clickReg();
       userCanReg.message.shouldBe(visible);

       sleep(5000);

    }

}