package test;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import static com.codeborne.selenide.Configuration.browser;
import static com.codeborne.selenide.Selenide.clearBrowserCookies;
import static com.codeborne.selenide.Selenide.close;


public class BaseConfig {
    final String blogUrl = "http://bob-blog.s-host.net/";

    @BeforeClass
    public void before() {
        browser = "chrome";
        Configuration.startMaximized = true;
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
        clearBrowserCookies();
    }

    @AfterClass
    public void after() {
        close();
    }
}


