package Kronas;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class Registration {
    public SelenideElement
    lkBtn = $x("//*[@id=\"bs-example-navbar-collapse-1\"]/ul[2]/a[2]"),//кнопка регистрации
    form = $x("/html/body/div/div/div/form"),
    nameField = $x("//*[@id=\"name\"]"),
    emailField = $x("//*[@id=\"email\"]"),//поле email
    passwordField = $x("//*[@id=\"password\"]"),//
    doublePassword = $x("//*[@id=\"password_confirmation\"]"),
    regBtn = $x("/html/body/div/div/div/form/input[6]"),
    message = $x("//*[@id=\"bs-example-navbar-collapse-1\"]/ul[2]/li/a");

    public void ClickLkBtn(){
        lkBtn.click();//кнопка личного кабинета
    }
    public void valnameField(String name){
        nameField.val(name);//ввод валидного имени
    }
    public void valemailField(String email){
        emailField.val(email);//ввод валидного email
    }
    public void valpasswordField(String password){
        passwordField.val(password);//ввод пароля
    }
    public void valdoublePas(String password)
    {doublePassword.val(password);}
    public void clickReg(){
        regBtn.click();//нажатие кнопки регистрации
    }
    //public void


}
